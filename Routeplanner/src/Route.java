import java.io.*;
import java.util.*;
class RoutePlanner{
	public static void main(String args[]) {
		
		String fileName="routes.csv";
		int n=getCountOfRoutes(fileName);
		String [][] arr=readRoutes(fileName, n); 
		Scanner sc= new Scanner(System.in);
		String source=sc.next();
		String dest=sc.next();
		showDirectFlights(arr, source );    
		sortDirectFlights(arr, dest);     
		showAllConnections(arr, source, dest);   
		
		
		
	}
	
	
   	

	public static void showDirectFlights(String[][] routesInfo, String fromCity) {
		for(String s[] : routesInfo) {
			if(s[0].equals(fromCity))
				 System.out.println(s[0]+" "+s[1]+" "+s[2]+" "+s[3]+" "+s[4]);
		}
	}
	
	
	
	public static void sortDirectFlights(String routesInfo[][], String fromCity) {
		sortArr(routesInfo);
		for(String s[] : routesInfo) {
			if(s[0].equals(fromCity))
				 System.out.println(s[0]+" "+s[1]+" "+s[2]+" "+s[3]+" "+s[4]);
		}
	}

	public static void showAllConnections(String routeInfo[][], String fromCity, String toCity) {
		for(String s[] : routeInfo) {
			if(s[0].equals(fromCity) && s[1].equals(toCity))
				System.out.println(s[0]+" "+s[1]+" "+s[2]+" "+s[3]+" "+s[4]);
			else if(s[0].equals(fromCity)) {
				showAllConnections(routeInfo,s[1],toCity);
			} 
		}
	}
	
	public static  void sortArr (String[][] array){
        Arrays.sort(array, new Comparator<String[]>() {
            @Override
            public int compare(String[] first, String[] second) {
               if(first[1].compareTo( second[1])>0) return 1;
               else return -1;
            }
        });
    }

	/********* method to get total no of routes from csv file *******/
	public static int getCountOfRoutes(String fileName) {
		int count=0;
		try {
				FileReader fr= new FileReader(fileName);
				BufferedReader bufferedReader= new BufferedReader(fr);
				while(bufferedReader.readLine() != null)
					   count++;
				bufferedReader.close();
				fr.close();
		     }
		catch(Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	

public static String[][] readRoutes(String fileName, int countOfRoutes){
	String[][] routes=new String[countOfRoutes][5];
	try {
		FileReader fr= new FileReader(fileName);
		BufferedReader bufferedReader= new BufferedReader(fr);
		for(int i=0;i<countOfRoutes;i++) {
		          String rawLine=bufferedReader.readLine();
		          String[] route= rawLine.split(",");
		          routes[i][0]=route[0];
		          routes[i][1]=route[1];
		          routes[i][2]=route[2];
		          routes[i][3]=route[3];
		          routes[i][4]=route[4];
		 	}
		bufferedReader.close();
		fr.close();
	}catch(Exception e) {
		e.printStackTrace();
	}
	return routes;
}
}